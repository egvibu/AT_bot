from sqlalchemy import create_engine
from sqlalchemy.orm import sessionmaker
from Airport import Airport, Base
import csv
import urllib.request

print('IMPORTING CODES ... ')

#url = "https://datahub.io/core/airport-codes/r/airport-codes.csv"
#response = urllib.request.urlopen(url)
#lines = [l.decode('utf-8') for l in response.readlines()]
file = open('airport-codes.csv', 'r')
lines = file.readlines()
file.close()

print('* CREATING ENGINE...')

engine = create_engine('mysql://root:pass@mydb:3306/', echo=True)
engine.execute('create database if not exists mydb')
engine = create_engine('mysql://root:pass@mydb:3306/mydb?charset=utf8mb4', echo=True)


Base.metadata.create_all(engine)
metadata = Base.metadata
airports_table = Airport.__table__

engine.execute("ALTER TABLE airports CONVERT TO CHARACTER SET utf8")

Session = sessionmaker(bind=engine)
s = Session()


reader = csv.reader(lines)
for row in reader:
    if len(row[11].split(',')) == 2:
        latitude, longitude = row[11].split(',')
        s.add(Airport(row[0], row[1], row[2], row[3], row[4], row[5], row[6], row[7], row[8], row[9], row[10],
                              latitude, longitude))
s.commit()

print("CODES IMPORTED")