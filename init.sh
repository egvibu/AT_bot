DATAFILE=/root/AT_bot/airport-codes.csv
if ! [ -f "$DATAFILE" ]; then
  wget --quiet https://datahub.io/core/airport-codes/r/airport-codes.csv -O $DATAFILE;
fi