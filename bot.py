import telebot
import re
from distance import distance
from search import search_by_code
from Airport import Airport

import sys
from sqlalchemy import create_engine
from sqlalchemy.orm import sessionmaker

print("BOT RUNNING2 ...")

engine = create_engine('mysql://root:pass@mydb:3306/mydb', echo=True)
Session_search = sessionmaker(bind=engine)
s = Session_search()
bot = telebot.TeleBot('1867803963:AAGrw-9Gvdiva8Da8c6c7-YrgiBHw1kD3_4')

@bot.message_handler(content_types=['text'])
def get_text_messages(message):
    if message.text == "Привет":
        bot.send_message(message.from_user.id, "Привет, чем я могу тебе помочь?")
    elif message.text == "/help":
        bot.send_message(message.from_user.id, "1.Введите код аэропорта, чтобы узнать его название \n2.Введите коды двух аэропортов, чтобы узнать расстояние между ними")
    elif re.match('.........', message.text):
        codes = message.text.split()
        dist = round(distance(codes[0], codes[1], Airport, s))
        if dist == -1:
            bot.send_message(message.from_user.id, "Код(ы) не найдены. Проверьте введённые данные")
        else:
            bot.send_message(message.from_user.id, dist)
    elif re.match('\w{4}', message.text ):
        info = search_by_code(message.text, Airport, s)
        if info is None:
            bot.send_message(message.from_user.id, "Код не найден. Проверьте введённые данные")
        else:
            bot.send_message(message.from_user.id, search_by_code(message.text, Airport, s)["name"])
    else:
        bot.send_message(message.from_user.id, "Я тебя не понимаю. Напиши /help.")

bot.polling(none_stop=True, interval=0)